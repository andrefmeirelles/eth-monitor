const functions = require('firebase-functions')
const admin = require('firebase-admin')
const _ = require('lodash')
const Web3 = require('web3')
const web3 = new Web3()
const { eth, providers } = web3

admin.initializeApp(functions.config().firebase)

const read = ({ body }, response) => {
    const { address, fields, infura_token, network, abi } = body
    web3.setProvider(new web3.providers.HttpProvider(`https://${network}.infura.io/${infura_token}`))
    const contract = new web3.eth.Contract(abi, address)
    const methods = _.map(fields, (method) => {
        return contract.methods[method]().call()
    })
    Promise.all(methods).then(
            (results) => {
                const result = _.reduce(fields, (memo, method, index) => {
                    memo[method] = results[index]
                    return memo
                }, {})
                return response.status(200).send(result)
        }).catch(
            (error) => {
                return response.status(500).send(error)
            }
        )
}

exports.contractState = functions.https.onRequest(read);
